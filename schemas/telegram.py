from pydantic import BaseModel


class TgSendMessageSchema(BaseModel):
    message: str
    entity: str
