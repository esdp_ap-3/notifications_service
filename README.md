<h1>Telethon based notifications microservice</h1>

<h2>Auth</h2>
<ol>
<li>
    Send post request to "http://localhost:8001/tg-auth/send_code/" <br>
    payload: {phone: str}
</li>
<li>
    Send post request to "http://localhost:8001/tg-auth/submit/" <br>
    payload: {phone: str, code: int}
    where "code" is received code of telegram chat
</li>
<li>
    if you get {"success": "True"} in the both cases => all good 
    else => check error msg from response
</li>
</ol>