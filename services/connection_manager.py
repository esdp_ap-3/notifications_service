from fastapi import WebSocket


class ConnectionManager:
    def __init__(self):
        self.connections: dict[int:WebSocket] = {}

    async def connect(self, websocket: WebSocket, user_id: int):
        await websocket.accept()
        self.connections[user_id] = websocket

    async def broadcast(self, data: str):
        for user, connection in self.connections.items():
            await connection.send_json(data)

    def disconnect(self, user_id: int):
        if user_id in self.connections:
            self.connections.pop(user_id)
