from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from api.tg_auth_router import router as tg_auth_router
from api.client_router import router as tg_client_router

from fastapi import WebSocket
from schemas.chat import TokenSchema
from services.jwt_token import encode
from db.connection import run_db_for_app
from services import jwt_token
from services.message_sender import send_message, get_message_structure, send_notification, get_notification_structure
from services.network_manager import NetworkManager, NotificationNetworkManager
from starlette.responses import Response
import logging

app = FastAPI(title='BikeCheck')

origins = ['*']
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)

app.include_router(tg_auth_router)
app.include_router(tg_client_router)


network_manager = NetworkManager()
notification_network_manager = NotificationNetworkManager()


@app.post(path='/token/')
async def get_jwt_token(schema: TokenSchema):
    token = await encode(schema.username, schema.password)

    if not token:
        return Response('token error', status_code=400)
    return {'jwt': token}


@app.websocket("/ws/{chat_id}/{token}")
async def websocket_endpoint(websocket: WebSocket, chat_id: int, token: str):
    flag, message_or_user = await jwt_token.is_correct_jwt(token, chat_id)
    if not flag:
        return {'error': message_or_user}

    user = message_or_user

    manager = network_manager.get_manager_or_create(chat_id)
    await manager.connect(websocket, user.id)
    try:
        while True:
            text = await websocket.receive_text()
            message, receiver = await send_message(message=text, user=user, chat_id=chat_id)
            await manager.broadcast(get_message_structure(user, text))
            try:
                if receiver.pk not in manager.connections:
                    notification, receiver = await send_notification(user=user, message=message, chat_id=chat_id)
                    await notification_network_manager.broadcast(get_notification_structure(user=user,
                                                                                            notification=notification,
                                                                                            chat_id=chat_id),
                                                                 receiver)
            except KeyError:
                pass
    finally:
        manager.disconnect(user.id)


@app.websocket("/notifications/{token}")
async def websocket_endpoint(websocket: WebSocket, token: str):
    decoded_data = await jwt_token.decode(token)
    if 'user' not in decoded_data:
        return Response('token error', status_code=400)

    user = decoded_data['user']
    await notification_network_manager.connect(websocket, user)
    try:
        while True:
            text = await websocket.receive_text()
    finally:
        notification_network_manager.disconnect(user)


run_db_for_app(app)
