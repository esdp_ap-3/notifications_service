from tortoise.models import Model
from tortoise import fields


class User(Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=255, unique=True)
    password = fields.CharField(max_length=255)
    first_name = fields.CharField(max_length=255)
    last_name = fields.CharField(max_length=255)
    email = fields.CharField(max_length=255, unique=True)
    telegram = fields.CharField(max_length=255, null=True)

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        table = 'accounts_user'

    def __str__(self):
        return self.get_full_name()


class Producer(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)

    class Meta:
        table = 'bikes_producer'

    def __str__(self):
        return f'Producer: {self.name}'


class Bike(Model):
    id = fields.IntField(pk=True)
    producer = fields.ForeignKeyField('app.Producer', related_name='bikes')
    model = fields.CharField(max_length=100)
    is_custom = fields.BooleanField()

    class Meta:
        table = 'bikes_bike'

    def __str__(self):
        return f'Bike: {self.model}'


class Chat(Model):
    id = fields.IntField(pk=True)
    post = fields.ForeignKeyField('app.Post', related_name='chats')
    creator = fields.ForeignKeyField('app.User', related_name='chats')

    class Meta:
        table = 'notifications_chat'

    def __str__(self):
        return f'{self.post} || {self.creator}'


class Message(Model):
    text = fields.TextField(max_length=2000)
    chat = fields.ForeignKeyField('app.Chat', related_name='messages')
    sender = fields.ForeignKeyField('app.User', related_name='senders')
    receiver = fields.ForeignKeyField('app.User', related_name='receivers')
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)
    is_deleted = fields.BooleanField(default=False)

    class Meta:
        table = 'notifications_message'

    def __str__(self):
        return f'{self.sender.username } || {self.text[:30]}'


class BikeHistory(Model):
    owner = fields.ForeignKeyField('app.User', related_name='history')
    bike = fields.ForeignKeyField('app.Bike', related_name='history')

    class Meta:
        table = 'bikes_bikehistory'

    def __str__(self):
        return f'{self.bike.model} | {self.owner.username}'


class Post(Model):
    owner = fields.ForeignKeyField('app.User', related_name='posts')
    bike = fields.ForeignKeyField('app.Bike', related_name='posts')

    class Meta:
        table = 'advertisement_post'

    def __str__(self):
        return f'POST: {self.bike.model} | {self.owner.username}'


class Notification(Model):
    user = fields.ForeignKeyField('app.User', related_name='notifications')
    text = fields.TextField(max_length=2000, null=True)
    message = fields.ForeignKeyField('app.Message', null=True, related_name='notifications')
    is_seen = fields.BooleanField(default=False)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)
    is_deleted = fields.BooleanField(default=False)

    class Meta:
        table = 'notifications_notification'

    def __str__(self):
        return f'Notification: {self.text[:20] if self.text else self.message[:20]}'
