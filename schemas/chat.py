from pydantic import BaseModel


class ChatMessageSchema(BaseModel):
    text: str
    chat: int
    receiver: int
    sender: int


class TokenSchema(BaseModel):
    username: str
    password: str
