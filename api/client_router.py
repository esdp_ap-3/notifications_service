from fastapi import APIRouter
from schemas.telegram import TgSendMessageSchema
from db.models import Notification, User
from services.telegram import Telegram


router = APIRouter(
    prefix='/telegram',
    tags=['tg']
)


@router.post(path='/send_msg/')
async def send_message(schema: TgSendMessageSchema):
    user = await User.get(telegram=schema.entity)
    await Notification.create(user=user, text=schema.message)
    return await Telegram().send_message(schema)
