from db.models import Bike, BikeHistory, Chat, Message, User, Post, Notification
import datetime


async def send_message(user: User, message: str, chat_id: int):
    chat: Chat = await Chat.get(id=chat_id)
    post: Post = await chat.post
    owner: User = await post.owner

    if await chat.creator == user:
        receiver = owner
    else:
        receiver = await chat.creator

    message = await Message.create(chat=chat, text=message, sender=user, receiver=receiver)
    return message, receiver


async def send_notification(user: User, message: Message, chat_id: int):
    chat: Chat = await Chat.get(id=chat_id)
    post: Post = await chat.post
    owner: User = await post.owner

    if await chat.creator == user:
        receiver = owner
    else:
        receiver = await chat.creator

    notification = await Notification.create(user=receiver, message=message)
    return notification, receiver


def get_message_structure(user: User, message: str):
    return {
        'user_id': str(user.id),
        'user': user.get_full_name(),
        'message': message,
        'time': get_message_formated_datetime(datetime.datetime.now())
    }


def get_notification_structure(user: User, notification: Notification, chat_id: int):
    return {
        'user': user.get_full_name(),
        'text': notification.text if notification.text else notification.message.text,
        'chat_id': chat_id,
    }


def get_message_formated_datetime(time):
    return time.strftime('%a, %d %b. %H:%m')
