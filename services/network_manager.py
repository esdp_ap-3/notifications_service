from services.connection_manager import ConnectionManager
from db.models import User
from fastapi import WebSocket


class NetworkManager:
    def __init__(self):
        self.network: {int: ConnectionManager} = {}

    def get_manager_or_create(self, chat_id: int):
        current_chat = self.network.get(chat_id)
        if not current_chat:
            self.network[chat_id] = ConnectionManager()
        manager = self.network[chat_id]
        return manager


class NotificationNetworkManager:
    def __init__(self):
        self.network: {User: WebSocket} = {}

    async def connect(self, websocket: WebSocket, user: User):
        await websocket.accept()
        self.network[user] = websocket

    async def broadcast(self, data: str, user: User):
        if user in self.network:
            connection = self.network[user]
            await connection.send_json(data)

    def disconnect(self, user: User):
        if user in self.network:
            self.network.pop(user)
