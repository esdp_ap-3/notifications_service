fastapi==0.70.1
telethon==1.24.0
uvicorn==0.16.0
websockets==10.1
tortoise-orm[asyncpg]==0.17.8
asyncpg==0.23.0
pyjwt==2.3.0
passlib==1.7.4