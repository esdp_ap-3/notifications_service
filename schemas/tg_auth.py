from typing import Union

from pydantic import BaseModel


class TgSendMsgSchema(BaseModel):
    phone: str


class TgSubmitCodeSchema(BaseModel):
    phone: str
    code: Union[str, int]
