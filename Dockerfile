FROM python:3.8-slim

ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /usr/src/tg
COPY ./requirements ./requirements

RUN apt-get update && apt-get upgrade -y && apt-get install gcc -y
RUN pip3 install --upgrade pip setuptools wheel && pip3 install -r requirements/base.txt

COPY . .
CMD [ "uvicorn", "main:app", "--reload", "--host=0.0.0.0", "--port=8001" ]
