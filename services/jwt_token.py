import jwt
import os
from jwt.exceptions import InvalidSignatureError, DecodeError
from db.models import Chat, User
from passlib.apps import django_context

JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY')
JWT_ALGORITHM = os.getenv('JWT_ALGORITHM')


async def encode(username: str, password: str):
    user = await User.get(username=username)

    if user:
        if django_context.verify(password, user.password):
            jwt_token = jwt.encode(
                {'username': username,
                 'password': password},
                JWT_SECRET_KEY,
                JWT_ALGORITHM
            )
            return jwt_token


async def decode(jwt_token: str):
    try:
        decoded_data = jwt.decode(jwt_token,
                                  JWT_SECRET_KEY,
                                  algorithms=[JWT_ALGORITHM])

        return {'user': await User.get(username=decoded_data['username'])}

    except (InvalidSignatureError, DecodeError) as e:
        return {'error': 'jwt token is invalid!'}


async def is_correct_jwt(token: str, chat_id: int):
    answer = await decode(token)

    if 'user' in answer:
        user = answer.get('user')
        chat = await Chat.get(id=chat_id)
        post = await chat.post
        bike = await post.bike
        history = await bike.history.all()
        owner = await history[0].owner

        if user == await chat.creator or user == owner:
            return True, user
        else:
            return False, 'connection forbidden!'

    else:
        return False, answer['error']
