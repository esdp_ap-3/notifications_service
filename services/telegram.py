import logging

from fastapi import Response
from starlette import status
from telethon import TelegramClient

from schemas.tg_auth import TgSendMsgSchema, TgSubmitCodeSchema
from schemas.telegram import TgSendMessageSchema
from config import tg_config as config


class Telegram:
    phone_code_hash = None

    def __init__(
            self,
            session_name: str = config.SESSION_NAME,
            api_id: int = config.API_ID,
            api_hash: str = config.API_HASH
    ) -> None:
        self.client = TelegramClient(session_name, api_id, api_hash)

    @staticmethod
    def none_phone_cache() -> str:
        """
        Phone cache can be left as None to use the
        last hash known for the phone to be used.
        """
        return 'None'

    async def send_message(self, schema: TgSendMessageSchema):
        try:
            async with self.client:
                await self.client.send_message(schema.entity, schema.message)

            return {'success': 'True'}

        except Exception as e:
            print(e)
            return {'success': 'False', 'details': e}

    async def login(self, schema: TgSendMsgSchema, response: Response):
        try:
            await self.client.connect()
            Telegram.phone_code_hash = await self.client.send_code_request(schema.phone)
            await self.client.disconnect()

            return {'success': 'True'}

        except Exception as e:
            print(e)
            response.status_code = status.HTTP_400_BAD_REQUEST
            return {'success': 'False', 'details': e}

    async def submit_login_code(self, schema: TgSubmitCodeSchema, response: Response):
        try:
            await self.client.connect()
            await self.client.sign_in(
                phone=schema.phone,
                code=schema.code,
                phone_code_hash=Telegram.phone_code_hash.phone_code_hash
            )
            await self.client.disconnect()

            return {'success': 'True', 'phone_tg': schema.phone}

        except Exception as e:
            print(e)
            response.status_code = status.HTTP_400_BAD_REQUEST
            return {'success': 'False', 'details': e}

    async def logout(self, response):
        try:
            async with self.client:
                if await self.client.is_user_authorized():
                    await self.client.log_out()
                    return {'success': 'True'}

                response.status_code = status.HTTP_400_BAD_REQUEST
                return {'success': 'False', 'details': 'You are already authorized!'}

        except Exception as e:
            print(e)
            response.status_code = status.HTTP_400_BAD_REQUEST
            return {'success': 'False', 'details': e}

    async def is_authorized(self):
        try:
            async with self.client:
                if await self.client.is_user_authorized():
                    me = await self.client.get_me()
                    return {'success': 'True', 'details': {
                        'username': me.username,
                        'number': me.phone
                    }}
                return {'success': 'False', 'details': 'You are already authorized!'}

        except Exception as e:
            return {'success': 'False', 'details': e}
