from fastapi import APIRouter, Response

from schemas.tg_auth import TgSendMsgSchema, TgSubmitCodeSchema
from services.telegram import Telegram

router = APIRouter(
    prefix='/tg-auth',
    tags=['tg-auth'],
)

phone_hash = None


@router.post(path='/send_code/')
async def send_code(schema: TgSendMsgSchema, response: Response):
    return await Telegram().login(schema, response)


@router.post(path='/submit/')
async def submit_code(schema: TgSubmitCodeSchema, response: Response):
    return await Telegram().submit_login_code(schema, response)


@router.get(path='/logout/')
async def logout(response: Response):
    return await Telegram().logout(response)


@router.get(path='/is_authorized/')
async def is_authorized():
    return await Telegram().is_authorized()
